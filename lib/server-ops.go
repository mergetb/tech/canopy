package canopy

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"
)

func SetLink(name string, value UpDown) error {

	fields := log.Fields{
		"name":  name,
		"value": UpDown_name[int32(value)],
	}
	log.WithFields(fields).Info("set-link")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	switch value {

	case UpDown_Up:
		err := lnk.Up(ctx)
		if err != nil {
			return ErrorEF("link up failed", err, fields)
		}

	case UpDown_Down:
		err := lnk.Down(ctx)
		if err != nil {
			return ErrorEF("link down failed", err, fields)
		}

	}

	return nil

}

func SetMtu(name string, value int32) error {

	fields := log.Fields{
		"name":  name,
		"value": value,
	}
	log.WithFields(fields).Info("set-mtu")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	err = lnk.SetMtu(ctx, int(value))
	if err != nil {
		return ErrorEF("set mtu failed", err, fields)
	}

	return nil

}

func SetAccess(name string, value *Access) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(value.Presence)],
		"vid":      value.Vid,
	}
	log.WithFields(fields).Info("set-access")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	if value.Presence == Presence_Absent {

		err = lnk.SetUntagged(ctx, uint16(value.Vid), true, true, false)
		if err != nil {
			return ErrorEF("set access failed", err, fields)
		}
		return nil

	}

	brmap, err := ifmap(ctx, unix.AF_BRIDGE)
	if err != nil {
		return err
	}

	brinfo, ok := brmap[lnk.Msg.Index]
	if ok {

		//remove existing untagged
		for _, v := range brinfo.Info.Untagged {
			err = lnk.SetUntagged(ctx, v, true, true, false)
			if err != nil {
				return ErrorEF("clear untagged failed", err, fields)
			}
		}

		//remove existing tagged
		for _, v := range brinfo.Info.Tagged {
			err = lnk.SetTagged(ctx, v, true, true, false)
			if err != nil {
				return ErrorEF("clear untagged failed", err, fields)
			}
		}

	}

	err = lnk.SetUntagged(ctx, uint16(value.Vid), false, true, false)
	if err != nil {
		return ErrorEF("set access failed", err, fields)
	}

	return nil

}

func SetTagged(name string, value *Tagged) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(value.Presence)],
		"vids":     fmt.Sprintf("%v", value.Vids),
	}
	log.WithFields(fields).Info("set-tagged")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	unset := false
	if value.Presence == Presence_Absent {
		unset = true
	}

	for _, vid := range value.Vids {

		err = lnk.SetTagged(ctx, uint16(vid), unset, false, false)
		if err != nil {
			return ErrorEF("set tagged failed", err, fields)
		}

	}

	return nil

}

func SetUntagged(name string, value *Untagged) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(value.Presence)],
		"vids":     fmt.Sprintf("%v", value.Vids),
	}
	log.WithFields(fields).Info("set-untagged")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	unset := false
	if value.Presence == Presence_Absent {
		unset = true
	}

	for _, vid := range value.Vids {

		err = lnk.SetUntagged(ctx, uint16(vid), unset, false, false)
		if err != nil {
			return ErrorEF("set untagged failed", err, fields)
		}

	}

	return nil

}

func SetMaster(name string, master *Master) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(master.Presence)],
		"master":   master.Value,
	}
	log.WithFields(fields).Info("set-master")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	switch master.Presence {

	case Presence_Present:

		br, err := rtnl.GetLink(ctx, master.Value)
		if err != nil {
			return ErrorE("getting master failed", err)
		}

		err = lnk.SetMaster(ctx, int(br.Msg.Index))
		if err != nil {
			return ErrorEF("setting master failed", err, fields)
		}

	case Presence_Absent:

		//TODO

	}

	return nil

}

func SetBridgePresence(name string, value Presence) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(value)],
	}
	log.WithFields(fields).Info("bridge-presence")

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return ErrorEF("open net-context failed", err, fields)
	}
	defer ctx.Close()

	lnk := rtnl.NewLink()
	lnk.Msg.Family = unix.AF_BRIDGE
	lnk.Info.Name = name
	lnk.Info.Bridge = &rtnl.Bridge{VlanAware: true}

	switch value {
	case Presence_Present:

		err = lnk.Present(ctx)
		if err != nil {
			return ErrorEF("ensure bridge present failed", err, fields)
		}

	case Presence_Absent:

		err = lnk.Absent(ctx)
		if err != nil {
			return ErrorEF("remove bridge failed", err, fields)
		}

	}

	return nil

}

func SetVtepPresence(name string, vtep *VtepPresence) error {

	fields := log.Fields{
		"name":     name,
		"presence": Presence_name[int32(vtep.Presence)],
		"vni":      vtep.Vni,
		"tunnelip": vtep.TunnelIp,
		"parent":   vtep.Parent,
	}
	log.WithFields(fields).Info("vtep-presence")

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return ErrorEF("open-context", err, fields)
	}
	defer ctx.Close()

	lnk := rtnl.NewLink()
	lnk.Info.Name = name

	switch vtep.Presence {
	case Presence_Present:

		lnk.Info.Vxlan = &rtnl.Vxlan{
			Vni:     uint32(vtep.Vni),
			Local:   net.ParseIP(vtep.TunnelIp),
			DstPort: 4789,
		}

		if vtep.Learning == OnOff_On {
			lnk.Info.Vxlan.Learning = 1
		}

		if vtep.Parent != "" {
			plnk, err := rtnl.GetLink(ctx, vtep.Parent)
			if err != nil {
				return ErrorEF("get-parent", err, fields)
			}
			lnk.Info.Vxlan.Link = uint32(plnk.Msg.Index)
		}

		err = lnk.Present(ctx)
		if err != nil {
			return ErrorEF("ensure vtep present failed", err, fields)
		}

	case Presence_Absent:

		err = lnk.Absent(ctx)
		if err != nil {
			return ErrorEF("remove vtep failed", err, fields)
		}

	}

	return nil

}

func SetLearning(name string, value OnOff) error {

	fields := log.Fields{
		"name":  name,
		"value": OnOff_name[int32(value)],
	}
	log.WithFields(fields).Info("vtep-presence")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	var learning uint8 = 0
	if value == OnOff_On {
		learning = 1
	}

	lnk.Info.Vxlan.Learning = learning
	err = lnk.Set(ctx)
	if err != nil {
		return ErrorEF("set learning failed", err, fields)
	}

	return nil

}

func SetVlanAware(name string, value bool) error {

	fields := log.Fields{
		"name":  name,
		"value": value,
	}
	log.WithFields(fields).Info("vlan-aware")

	ctx, lnk, err := getLink(name)
	defer ctx.Close()
	if err != nil {
		return err
	}

	lnk.Info.Bridge.VlanAware = value
	err = lnk.Set(ctx)
	if err != nil {
		return ErrorEF("set vlan aware failed", err, fields)
	}

	return nil
}

func getLink(name string) (*rtnl.Context, *rtnl.Link, error) {

	fields := log.Fields{
		"name": name,
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return nil, nil, ErrorEF("open net-context failed", err, fields)
	}

	lnk, err := rtnl.GetLink(ctx, name)
	if err != nil {
		return nil, nil, ErrorEF("link not found", err, fields)
	}

	return ctx, lnk, nil

}
