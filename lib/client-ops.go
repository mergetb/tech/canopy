package canopy

// link ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetLink(value UpDown, name, host string) *Client {

	return c.add(&Property_Link{value}, name, host)

}

func (c *Client) SetLinks(value UpDown, names, hosts []string) *Client {

	return c.addMany(&Property_Link{value}, names, hosts)
}

// mtu ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetMtu(value int32, name, host string) *Client {

	return c.add(&Property_Mtu{value}, name, host)

}

func (c *Client) SetMtus(value int32, names, hosts []string) *Client {

	return c.addMany(&Property_Mtu{value}, names, hosts)

}

// access ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetAccessPort(value *Access, name, host string) *Client {

	return c.add(&Property_Access{value}, name, host)

}

func (c *Client) SetAccessPorts(value *Access, names, hosts []string) *Client {

	return c.addMany(&Property_Access{value}, names, hosts)

}

// tagged ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetTaggedPort(value *Tagged, name, host string) *Client {

	return c.add(&Property_Tagged{value}, name, host)

}

func (c *Client) SetTaggedPorts(value *Tagged, names, hosts []string) *Client {

	return c.addMany(&Property_Tagged{value}, names, hosts)

}

// untagged ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetUntaggedPort(value *Untagged, name, host string) *Client {

	return c.add(&Property_Untagged{value}, name, host)

}

func (c *Client) SetUntaggedPorts(value *Untagged, names, hosts []string) *Client {

	return c.addMany(&Property_Untagged{value}, names, hosts)

}

// master ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetMaster(value *Master, name, host string) *Client {

	return c.add(&Property_Master{value}, name, host)

}

func (c *Client) SetMasters(value *Master, names, hosts []string) *Client {

	return c.addMany(&Property_Master{value}, names, hosts)

}

// learning ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetLearningVtep(value OnOff, name, host string) *Client {

	return c.add(&Property_Learning{value}, name, host)

}

func (c *Client) SetLearningVteps(value OnOff, names, hosts []string) *Client {

	return c.addMany(&Property_Learning{value}, names, hosts)

}

// vlan aware ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetVlanAwareBridge(v bool, name, host string) *Client {

	return c.add(&Property_VlanAware{v}, name, host)

}

func (c *Client) SetVlanAwareBridges(v bool, names, hosts []string) *Client {

	return c.addMany(&Property_VlanAware{v}, names, hosts)

}

// bridge presence ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetBridgePresent(p Presence, name, host string) *Client {

	return c.add(&Property_Bridge{p}, name, host)

}

func (c *Client) SetBridgesPresent(p Presence, names, hosts []string) *Client {

	return c.addMany(&Property_Bridge{p}, names, hosts)

}

// vtep presence ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *Client) SetVtep(v *VtepPresence, name, host string) *Client {

	return c.add(&Property_Vtep{v}, name, host)

}

func (c *Client) SetVteps(v *VtepPresence, names, hosts []string) *Client {

	return c.addMany(&Property_Vtep{v}, names, hosts)

}

// add ========================================================================

func (c *Client) add(pval isProperty_Value, name, host string) *Client {

	return c.Add(&Property{Element: name, Value: pval}, host)

}

func (c *Client) Add(property *Property, host string) *Client {

	_, ok := c.Pending[host]
	if !ok {
		c.Pending[host] = make(map[string][]*Property)
	}

	name := property.Element
	property.Order = int32(c.Counter)
	c.Pending[host][name] = append(c.Pending[host][name], property)
	c.Counter += 1

	return c

}

// add many ===================================================================

func (c *Client) addMany(pval isProperty_Value, names, hosts []string) *Client {

	return c.AddMany(&Property{Value: pval}, names, hosts)

}

func (c *Client) AddMany(property *Property, names, hosts []string) *Client {

	for _, host := range hosts {
		for _, name := range names {
			// create a copy of the property wrapper object to give it a unique
			// element name and order
			c.Add(&Property{
				Element: name,
				Order:   int32(c.Counter),
				Value:   property.Value,
			}, host)

			c.Counter += 1
		}
	}

	return c

}

// get ========================================================================

func (c *Client) GetPort(name, host string) *Client {

	add := true
	for _, x := range c.Queries[host] {
		if name == x {
			add = false
			break
		}
	}
	if add {
		c.Queries[host] = append(c.Queries[host], name)
	}

	return c

}

func (c *Client) GetPorts(names []string, host string) *Client {

	for _, name := range names {
		c.GetPort(name, host)
	}

	return c

}
