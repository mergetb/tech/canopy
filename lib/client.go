package canopy

import (
	"context"
	"fmt"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

type Elements map[string]map[string]*Element

type Client struct {
	Counter  int
	Pending  CommandSet
	Queries  QuerySet
	Elements Elements
}

func (es Elements) Set(host, port string, e *Element) {

	_, ok := es[host]
	if !ok {
		es[host] = make(map[string]*Element)
	}

	es[host][port] = e

}

var defaultPort string = "6074"

// hosts[id][port][]Elements
type CommandSet map[string]map[string][]*Property

// hosts[id][]port
type QuerySet map[string][]string

func init() {

	port := os.Getenv("CANOPY_SERVER_PORT")
	if port != "" {
		defaultPort = port
	}

}

func (c *Client) Commit() error {

	failedHosts := make(map[string]string)
	success := make(map[string]string)
	var mtx sync.Mutex

	var wg sync.WaitGroup

	for host, ports := range c.Pending {
		wg.Add(1)

		go func(host string, ports map[string][]*Property) {
			var pes []*Property
			for port, properties := range ports {
				for _, e := range properties {

					pes = append(pes, &Property{
						Order:   e.Order,
						Element: port,
						Value:   e.Value,
					})

				}
			}
			var txnid string
			err := SendSet(host, &SetRequest{Properties: pes})
			if err != nil {
				mtx.Lock()
				failedHosts[host] = err.Error()
				mtx.Unlock()
			} else {
				mtx.Lock()
				success[host] = txnid
				mtx.Unlock()
			}
			wg.Done()
		}(host, ports)

	}

	wg.Wait()

	if len(failedHosts) > 0 {
		errstr := ""
		for h, err := range failedHosts {
			errstr += fmt.Sprintf("%s: %s", h, err)
		}
		return fmt.Errorf(errstr)
	}

	return nil

}

func (c *Client) Query() error {

	failedHosts := make(map[string]string)
	success := make(map[string]string)
	var mtx sync.Mutex

	var wg sync.WaitGroup

	for host, ports := range c.Queries {
		wg.Add(1)

		go func(host string, ports []string) {

			var txnid string
			elements, err := SendGet(host, ports)
			if err != nil {
				mtx.Lock()
				failedHosts[host] = err.Error()
				mtx.Unlock()
			} else {
				mtx.Lock()
				success[host] = txnid
				for _, x := range elements {
					c.Elements.Set(host, x.Name, x)
				}
				mtx.Unlock()
			}
			wg.Done()
		}(host, ports)

	}

	wg.Wait()

	if len(failedHosts) > 0 {
		errstr := ""
		for h, err := range failedHosts {
			errstr += fmt.Sprintf("%s: %s", h, err)
		}
		return fmt.Errorf(errstr)
	}

	return nil

}

func NewClient() *Client {
	return &Client{
		Pending:  make(CommandSet),
		Queries:  make(QuerySet),
		Elements: make(Elements),
	}
}

func SendSet(host string, set *SetRequest) error {

	var endpoint string
	if strings.Contains(host, ":") {
		endpoint = host
	} else {
		endpoint = fmt.Sprintf("%s:%s", host, defaultPort)
	}

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()

	client := NewCanopyClient(conn)

	_, err = client.Set(context.TODO(), set)
	if err != nil {
		// unwrap grpc error
		s, ok := status.FromError(err)
		if ok {
			err = fmt.Errorf(s.Message())
		}
		return err
	}

	return nil

}

func ListPorts(hosts []string) (map[string][]*Element, error) {

	var rmtx, emtx sync.Mutex
	var wg sync.WaitGroup
	result := make(map[string][]*Element)
	var errors []string

	for _, host := range hosts {

		wg.Add(1)

		go func(host string) {

			defer wg.Done()

			var endpoint string
			if strings.Contains(host, ":") {
				endpoint = host
			} else {
				endpoint = fmt.Sprintf("%s:%s", host, defaultPort)
			}

			conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
			if err != nil {
				emtx.Lock()
				errors = append(errors, err.Error())
				emtx.Unlock()
				return
			}
			defer conn.Close()

			client := NewCanopyClient(conn)

			resp, err := client.List(context.TODO(), &ListRequest{})
			if err != nil {
				emtx.Lock()
				errors = append(errors, err.Error())
				emtx.Unlock()
				return
			}

			rmtx.Lock()
			result[host] = append(result[host], resp.Elements...)
			rmtx.Unlock()

		}(host)

	}

	wg.Wait()

	if len(errors) > 0 {
		return result, fmt.Errorf(strings.Join(errors, "\n"))
	}

	return result, nil

}

func SendGet(host string, ports []string) ([]*Element, error) {

	var endpoint string
	if strings.Contains(host, ":") {
		endpoint = host
	} else {
		endpoint = fmt.Sprintf("%s:%s", host, defaultPort)
	}

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	client := NewCanopyClient(conn)

	resp, err := client.Get(context.TODO(), &GetRequest{Names: ports})
	if err != nil {
		return nil, err
	}

	return resp.Elements, nil

}

/*
func SendGets(ports, hosts []string, e []string) (
	map[string][]*Element, map[string]error) {

	results := make(map[string][]*Element)
	errors := make(map[string]error)
	for _, h := range hosts {
		r, e := SendGet(ports, h, e)
		results[h] = r
		errors[h] = e
	}

	return results, errors

}
*/

func ParsePresence(s string) (Presence, error) {
	switch strings.ToLower(s) {
	case strings.ToLower(Presence_name[int32(Presence_Present)]):
		return Presence_Present, nil
	case strings.ToLower(Presence_name[int32(Presence_Absent)]):
		return Presence_Absent, nil
	default:
		return Presence_PresenceReserved, fmt.Errorf(
			"%s is not a valid presence value, must be 'absent' or 'present'", s)
	}
}

func ParseOnOff(s string) (OnOff, error) {
	switch strings.ToLower(s) {
	case strings.ToLower(OnOff_name[int32(OnOff_On)]):
		return OnOff_On, nil
	case strings.ToLower(OnOff_name[int32(OnOff_Off)]):
		return OnOff_Off, nil
	default:
		return OnOff_OnOffReserved, fmt.Errorf(
			"%s is not a valid on/off value, must be 'on' or 'off'", s)
	}
}

func ParseLinkState(s string) (UpDown, error) {
	switch strings.ToLower(s) {
	case strings.ToLower(UpDown_name[int32(UpDown_Up)]):
		return UpDown_Up, nil
	case strings.ToLower(UpDown_name[int32(UpDown_Down)]):
		return UpDown_Down, nil
	default:
		return UpDown_UpDownReserved, fmt.Errorf(
			"%s is not a valid UpDown value, must be 'up' or 'down'", s)
	}
}

func sortedMapKeys(m interface{}) (keyList []string) {
	keys := reflect.ValueOf(m).MapKeys()

	for _, key := range keys {
		keyList = append(keyList, key.Interface().(string))
	}
	sort.Strings(keyList)
	return
}
