package canopy

import (
	"context"
	"flag"
	"net"
	"sort"
	"sync"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var endpoint = flag.String("endpoint", "0.0.0.0:6074", "canopy server endpoint address")

func init() { flag.Parse() }

type Server struct {
	mux sync.Mutex
}

// Starts the canopy server and listens for requests.
func (s *Server) Run() {
	grpcServer := grpc.NewServer()
	RegisterCanopyServer(grpcServer, s)

	l, err := net.Listen("tcp", *endpoint)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Infof("listening on tcp://%s", *endpoint)
	log.Fatal(grpcServer.Serve(l))
}

// Implements Canopy::Set gRPC interface
func (s *Server) Set(ctx context.Context, req *SetRequest) (
	*SetResponse, error) {

	s.mux.Lock()
	defer s.mux.Unlock()

	err := s.set(req.Properties)
	if err != nil {
		//return nil, ErrorE("set failed", err)
		return nil, err
	}

	return &SetResponse{}, nil
}

// Implements Canopy::Get gRPC interface
func (s *Server) Get(ctx context.Context, req *GetRequest) (
	*GetResponse, error) {

	s.mux.Lock()
	defer s.mux.Unlock()

	elems, err := s.get(req.Names)
	if err != nil {
		return nil, ErrorE("get failed", err)
	}

	return &GetResponse{Elements: elems}, nil
}

func (s *Server) List(ctx context.Context, req *ListRequest) (
	*ListResponse, error) {

	elems, err := List(Any)
	if err != nil {
		return nil, ErrorE("list failed", err)
	}

	return &ListResponse{Elements: elems}, nil

}

func (s *Server) set(props []*Property) error {

	log.Debugf("setting %d properties", len(props))

	sort.Slice(props, func(i, j int) bool {
		return props[i].Order < props[j].Order
	})

	for _, e := range props {
		switch x := e.Value.(type) {

		// link
		case *Property_Link:
			err := SetLink(e.Element, x.Link)
			if err != nil {
				return err
			}

		case *Property_Mtu:
			err := SetMtu(e.Element, x.Mtu)
			if err != nil {
				return err
			}

		case *Property_Access:
			err := SetAccess(e.Element, x.Access)
			if err != nil {
				return err
			}

		case *Property_Tagged:
			err := SetTagged(e.Element, x.Tagged)
			if err != nil {
				return err
			}

		case *Property_Untagged:
			err := SetUntagged(e.Element, x.Untagged)
			if err != nil {
				return err
			}

		case *Property_Master:
			err := SetMaster(e.Element, x.Master)
			if err != nil {
				return err
			}

		// vtep
		case *Property_Vtep:
			err := SetVtepPresence(e.Element, x.Vtep)
			if err != nil {
				return err
			}

		case *Property_Learning:
			err := SetLearning(e.Element, x.Learning)
			if err != nil {
				return err
			}

		// bridge
		case *Property_Bridge:
			err := SetBridgePresence(e.Element, x.Bridge)
			if err != nil {
				return err
			}

		case *Property_VlanAware:
			err := SetVlanAware(e.Element, x.VlanAware)
			if err != nil {
				return err
			}

		}
	}

	return nil

}

/* get internal implementation */
func (s *Server) get(names []string) ([]*Element, error) {

	log.WithFields(log.Fields{"Names": names}).Debug("get")

	//TODO it's not clear to me whether 1 kernel call to list everything and then
	//     filter later or multiple kernel calls to specific things is more
	//     efficient. OTOH, i recall a recent netdev presentation from the cumulus
	//     folks talking about passing filters to netdev, so perhaps that is the
	//     ultimate solution here

	filter := func(e *Element) bool {
		for _, x := range names {
			if e.Name == x {
				return true
			}
		}
		return false
	}

	elements, err := List(filter)

	if err != nil {
		return nil, err
	}

	return elements, nil

}
