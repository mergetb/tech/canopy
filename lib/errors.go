package canopy

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"runtime"
)

func ErrorE(message string, err error) error {

	_, file, line, _ := runtime.Caller(1)

	log.
		WithError(err).
		WithFields(log.Fields{"caller": fmt.Sprintf("%s:%d", file, line)}).
		Error(message)

	return fmt.Errorf(message)

}

func ErrorEF(message string, err error, fields log.Fields) error {

	_, file, line, _ := runtime.Caller(1)

	fields["caller"] = fmt.Sprintf("%s:%d", file, line)

	log.
		WithError(err).
		WithFields(fields).
		Error(message)

	return fmt.Errorf(message)

}
