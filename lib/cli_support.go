package canopy

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"

	"github.com/golang/protobuf/proto"
	"gopkg.in/yaml.v2"
)

func (c *Client) SavePending() error {

	err := c.SavePendingCommands()
	if err != nil {
		return err
	}

	err = c.SavePendingQueries()
	if err != nil {
		return err
	}

	return nil

}

func (c *Client) SavePendingQueries() error {

	buf, err := json.MarshalIndent(c.Queries, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to serialize commands: %v", err)
	}

	qf, err := queryFile()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(qf, buf, 0644)
	if err != nil {
		return fmt.Errorf("failed to save pending command: %v", err)
	}

	return nil

}

func (c *Client) SavePendingCommands() error {

	// protobufs have a complex internal structure, we cannot simply serialize
	// them to and fro from json. We need to use the protobuf library marshalling
	// routines
	data := make(map[string]map[string][][]byte)

	addElem := func(host, port string, e []byte) {
		_, ok := data[host]
		if !ok {
			data[host] = make(map[string][][]byte)
		}
		data[host][port] = append(data[host][port], e)
	}

	cmds := c.Pending
	for hk, hv := range cmds {
		for pk, pv := range hv {
			for _, e := range pv {
				buf, err := proto.Marshal(e)
				if err != nil {
					return err
				}
				addElem(hk, pk, buf)
			}
		}
	}

	buf, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to serialize commands: %v", err)
	}

	pf, err := pendingFile()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(pf, buf, 0644)
	if err != nil {
		return fmt.Errorf("failed to save pending command: %v", err)
	}

	return nil

}

func pendingFile() (string, error) {

	dir, err := canopyDir()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/pending.json", dir), nil

}

func queryFile() (string, error) {

	dir, err := canopyDir()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/query.json", dir), nil

}

func canopyDir() (string, error) {

	u, err := user.Current()
	if err != nil {
		return "", fmt.Errorf("you are not a thing")
	}

	dir := fmt.Sprintf("%s/.canopy", u.HomeDir)

	err = os.MkdirAll(dir, 0755)
	if err != nil {
		return "", err
	}

	return dir, nil

}

func LoadPendingQueries(cli *Client) error {

	qry := make(QuerySet)
	qf, err := queryFile()
	if err != nil {
		return err
	}
	cli.Queries = qry

	buf, err := ioutil.ReadFile(qf)
	if err != nil {
		return nil
	}
	err = json.Unmarshal(buf, &qry)
	if err != nil {
		return err
	}

	cli.Queries = qry
	return nil

}

func LoadPendingCommands(cli *Client) error {

	var data map[string]map[string][][]byte
	cmds := make(map[string]map[string][]*Property)
	cli.Pending = cmds

	pf, err := pendingFile()
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadFile(pf)
	if err != nil {
		return nil
	}
	err = json.Unmarshal(buf, &data)
	if err != nil {
		return err
	}

	counter := 0

	addElem := func(host, port string, e *Property) {
		counter += 1
		_, ok := cmds[host]
		if ok {
			_, ok := cmds[host][port]
			if ok {
				cmds[host][port] = append(cmds[host][port], e)
			} else {
				cmds[host][port] = []*Property{e}
			}
		} else {
			cmds[host] = make(map[string][]*Property)
			cmds[host][port] = []*Property{e}
		}
	}

	for hk, hv := range data {
		for pk, pv := range hv {
			for _, buf := range pv {
				e := &Property{}
				err = proto.Unmarshal(buf, e)
				if err != nil {
					return err
				}
				addElem(hk, pk, e)
			}
		}
	}
	if err != nil {
		return fmt.Errorf("failed to parse pending commands: %v", err)
	}

	cli.Pending = cmds
	cli.Counter = counter
	return nil

}

func LoadClient() (*Client, error) {

	c := NewClient()
	err := LoadPendingCommands(c)
	if err != nil {
		return nil, err
	}

	err = LoadPendingQueries(c)
	if err != nil {
		return nil, err
	}

	return c, nil

}

func ClearPending() error {

	pf, err := pendingFile()
	if err != nil {
		return err
	}
	err = os.RemoveAll(pf)
	if err != nil {
		return err
	}

	qf, err := queryFile()
	if err != nil {
		return err
	}
	err = os.RemoveAll(qf)
	if err != nil {
		return err
	}

	return nil

}

func ShowResult(data interface{}) {

	buf, err := yaml.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(buf))

}

func ShowErrors(errors map[string]error) {
	for host, err := range errors {
		if err != nil {
			log.Printf("%s: %s", host, err)
		}
	}
}
