# Simple Virtual Network Test Cases

This directory contains some simple network virtualization test cases.

## Virtual Networks with Physical Interfaces

This test case covers materializing virtual networks composed of **only** physical interfaces. In the diagram below, the network on the right composed of three nodes is materialized over the topology shown on the left. Note that node `a` requires two connections, but the machine only has one interface. In this case the single physical interface maps into two network virtual edges (NVE). This allows the single interface to communicate over multiple virtual links. This is clearly not a viable solution for all experimentation scenarios, but for those where it is, this is probably the simplest encapsulation strategy for a node with fewer interfaces than the number of assigned virtual links. On a technical note, the NVEs are VXLAN virtual tunnel endpoints (VTEP). Each VTEP encapsulates the traffic it ingests in a VXLAN packet and pushes it upstream (visa vis BGP/EVPN). Nested encapsulated is fair game to send through these endpoints including VXLAN or VLAN.

The numbers in red indicate VLAN-isolated access channels. Note that these are pure access channels (not trunks) so packet tagging is not required for the virtual network. This is important, because to be able to support VLANs as a part of experiments transparently. No tagging, means experiment nodes are free to tag their packets in whatever way they wish and the virtualization mechanisms will not interfere.

![](img/simple-physical.png)

## Virtual Networks with Virtual Interfaces

### Without OS Virtualization

### With Containers

### With Virtual Machines
