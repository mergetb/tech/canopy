#!/bin/bash

net add vxlan vni-1010 vxlan id 1010
net add vxlan vni-1010 bridge access 10
net add vxlan vni-1010 vxlan local-tunnelip 172.0.3.1
net add vxlan vni-1010 bridge learning off
net add vxlan vni-1010 mtu 9152

net add vxlan vni-1011 vxlan id 1011
net add vxlan vni-1011 bridge access 11
net add vxlan vni-1011 vxlan local-tunnelip 172.0.3.1
net add vxlan vni-1011 bridge learning off
net add vxlan vni-1011 mtu 9152

net add interface swp1 bridge access 10
net add interface swp2 bridge access 11

net commit
