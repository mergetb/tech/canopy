#!/bin/bash
# SPINE

# interfaces
net add loopback lo ip address 172.0.1.1/24
net add interface swp1 ip address 10.0.0.1/30
net add interface swp2 ip address 10.0.0.5/30

# routing
net add bgp autonomous-system 64500
net add bgp router-id 10.0.0.1
net add bgp neighbor 10.0.0.2 remote-as 64501
net add bgp evpn neighbor 10.0.0.2 activate
net add bgp neighbor 10.0.0.6 remote-as 64502
net add bgp evpn neighbor 10.0.0.6 activate
net add bgp redistribute connected

net commit
