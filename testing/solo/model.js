
function cumulus(name) {
  return {
    'name': name,
    'image': 'cumulusvx-3.5-mvrf',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) },
    'mounts': [{ 'source': env.PWD+'/../../../canopy', 'point': '/tmp/canopy' }]
  };
}

topo = {
  name: 'canopy-solo',
  nodes: [],
  switches: [cumulus('a'), cumulus('b'), cumulus('c'), cumulus('d')],
  links: [
    Link('a', 1, 'b', 1),
    Link('a', 2, 'c', 1),
    Link('a', 3, 'd', 1)
  ]
}
