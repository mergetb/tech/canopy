#!/bin/bash

set -e

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip site0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/site0-1.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip site0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/site0-3.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip m0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/m0-1.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip m9` \
  /tmp/canopy/testing/dcomp-test/tests/test0/m9-1.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip site0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/site0-2.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip site0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/site0-4.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip site0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/site0-5.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip m0` \
  /tmp/canopy/testing/dcomp-test/tests/test0/m0-2.py

avocado run \
  --remote-username rvn \
  --remote-password rvn \
  --remote-hostname `rvn ip m9` \
  /tmp/canopy/testing/dcomp-test/tests/test0/m9-2.py
