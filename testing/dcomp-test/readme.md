# DComp Test

This folder contains a series of tests on a [DComp](https://dcomptb.net) like topology depicted below.

![](diagram.png)

These testing covers

- VLAN functionality on the control network (green switches)
- VXLAN functionality on the experiment network (blue switches)
- Basic port control


