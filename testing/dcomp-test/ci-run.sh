#!/bin/bash

set -x
set -e

if [[ $(id -u) -ne 0 ]]; then
  echo "must be root to run this script"
  exit 1;
fi

rvn destroy
rvn build
rvn deploy
rvn pingwait \
  m0 m1 m2 m3 m4 m5 m6 m7 m8 m9 m10 m11 \
  n0 n1 n2 n3 \
  xa0 xa1 xa2 xa3 \
  xf0 xf1 \
  csp0 \
  ca0 ca1 ca2 ca3 \
  cf0 cf1 \
  xsp0 \
  site0 site1
rvn configure

./test0.sh
