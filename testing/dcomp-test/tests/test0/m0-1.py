from avocado import Test
from avocado.utils import process

class Test0_Vlan_M0_1(Test):

    def test(s):
        process.system('sudo ip link set up eth1')
        process.system('sudo ip link set up eth2')

        # control net
        process.system('sudo ip addr add 10.0.0.1/24 dev eth1', 
                       ignore_status=True)

        # experiment net
        process.system('sudo ip addr add 10.99.0.1/24 dev eth2', 
                       ignore_status=True)
