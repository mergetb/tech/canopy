from avocado import Test
from avocado.utils import process

class Test0_Vlan_M9_1(Test):

    def test(s):
        process.system('sudo ip link set up eth1')
        process.system('sudo ip link set up eth2')

        # control net 
        process.system('sudo ip addr add 10.0.0.2/24 dev eth1',
                       ignore_status=True)
        rc = process.system('ping -c 1 10.0.0.1', ignore_status=True)
        #s.assertNotEqual(rc, 0)

        # experiment net
        process.system('sudo ip addr add 10.99.0.2/24 dev eth2',
                       ignore_status=True)
        rc = process.system('ping -c 1 10.99.0.1', ignore_status=True)
        #s.assertNotEqual(rc, 0)

