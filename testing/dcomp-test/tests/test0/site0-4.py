from avocado import Test
from avocado.utils import process

class Test0_Vlan_Site0_4(Test):

    def test(s):

        process.system('canopy abort')

        ### create vteps on xa0 and xa3
        process.system('canopy set bridge present bridge xa0,xa3')
        process.system('canopy set port link up bridge xa0,xa3')

        process.system('canopy set vtep present 1010 10.1.0.4 vni-1010 xa0')
        process.system('canopy set vtep present 1010 10.1.0.7 vni-1010 xa3')
        process.system('canopy set port link up vni-1010 xa0,xa3')

        process.system('canopy set port master bridge vni-1010 xa0,xa3')
        process.system('canopy set port access 10 vni-1010 xa0,xa3')

        process.system('canopy set port master bridge swp1 xa0,xa3')
        process.system('canopy set port access 10 swp1 xa0,xa3')
        process.system('canopy set port mtu 9000 vni-1010 xa0,xa3')
        process.system('canopy commit')
