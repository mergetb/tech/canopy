from avocado import Test
from avocado.utils import process

class Test0_Vlan_Site0_2(Test):

    def test(s):

        process.system('canopy abort')
        #process.system('canopy set bridge present br ca0,ca3,cf0,cf1,csp0')
        process.system('canopy set port link up bridge ca0,ca3,cf0,cf1,csp0')
        # access switches
        process.system('canopy set port master bridge swp1 ca0,ca3')
        process.system('canopy set port master bridge swp5 ca0,ca3')
        process.system('canopy set port access 47 swp1 ca0,ca3')
        process.system('canopy set port tagged 47 swp5 ca0,ca3')
        # fabric switches
        process.system('canopy set port master bridge swp1,swp3 cf0')
        process.system('canopy set port master bridge swp2,swp3 cf1')
        process.system('canopy set port tagged 47 swp1,swp3 cf0')
        process.system('canopy set port tagged 47 swp2,swp3 cf1')
        # spine
        process.system('canopy set port master bridge swp1,swp2 csp0')
        process.system('canopy set port tagged 47 swp1,swp2 csp0')

        process.system('canopy commit')
