import json
from avocado import Test
from avocado.utils import process

class Test0_Vtep_Site0(Test):

    def test(s):

        process.system('canopy abort')
        process.system('canopy set vtep present 4700 10.1.0.99 vtep4700 site0 -p lo')
        process.system('canopy set port master mzbr vtep4700 site0')
        process.system('canopy set port access 47 vtep4700 site0')
        process.system('canopy commit')

        out = process.system_output('ip -d -j link show vtep4700')
        js = json.loads(out)

        s.assertEqual(js[0]['linkinfo']['info_data']['id'], 4700)
        s.assertEqual(js[0]['linkinfo']['info_data']['link'], 'lo')
        s.assertEqual(js[0]['master'], 'mzbr')

