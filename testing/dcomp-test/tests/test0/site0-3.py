from avocado import Test
from avocado.utils import process

class Test0_Vlan_Site0_3(Test):

    def test(s):
        process.system('canopy abort')
        # access link & mtu
        process.system(
            'canopy set port link up swp1,swp2,swp3,swp4,swp5,swp6 xa0,xa1,xa2,xa3')
        process.system(
            'canopy set port mtu 9216 swp1,swp2,swp3,swp4,swp5,swp6 xa0,xa1,xa2,xa3')
        # fabric link & mtu
        process.system('canopy set port link up  swp1,swp2,swp3,swp4 xf0,xf1')
        process.system('canopy set port mtu 9216 swp1,swp2,swp3,swp4 xf0,xf1')
        # spine link & mtu
        process.system('canopy set port link up  swp1,swp2,swp3 xsp0')
        process.system('canopy set port mtu 9162 swp1,swp2,swp3 xsp0')

        # clear resident vni of interest
        process.system('canopy set vtep absent vni-1010 xa0,xa3')

        process.system('canopy commit')



