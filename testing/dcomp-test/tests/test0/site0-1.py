from avocado import Test
from avocado.utils import process

class Test0_Vlan_Site0_1(Test):

    def test(s):
        #clear any existing commands
        process.system('canopy abort')
        process.system('canopy set port link up swp1,swp2,swp3,swp4,swp5 ca0,ca3')
        process.system('canopy set port link up swp1,swp2,swp3,swp4 cf0,cf1')
        process.system('canopy set port link up swp1,swp2,swp3 csp0')

        # clear any existing bridging
        #process.system('canopy set bridge absent br ca0,ca3,cf0,cf1,csp0')

        process.system('canopy commit')


