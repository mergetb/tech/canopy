function cumulus(name) {
  return {
    'name': name,
    'image': 'cumulusvx-3.7',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': MB(512) },
    'mounts': [{ 'source': env.PWD+'/../../../canopy', 'point': '/tmp/canopy' }]
  };
}

function buster(name) {
  return {
    'name': name,
    'image': 'debian-buster',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) },
    'mounts': [{ 'source': env.PWD+'/../../../canopy', 'point': '/tmp/canopy' }]
  };
}

minnows = Range(12).map(i => buster('m'+i));
ncps = Range(4).map(i => buster('n'+i));
sites = Range(2).map(i => buster('site'+i));
stors = Range(2).map(i => buster('stor'+i));
emus = Range(2).map(i => buster('emu'+i));

xas = Range(4).map(i => cumulus('xa'+i));
cas = Range(4).map(i => cumulus('ca'+i));
xfs = Range(2).map(i => cumulus('xf'+i));
cfs = Range(2).map(i => cumulus('cf'+i));
xsp = cumulus('xsp0');
csp = cumulus('csp0');
gw = cumulus('gw');


cap = Range(4).map(i => 1);
xap = Range(4).map(i => 1);
cfp = Range(2).map(i => 1);
xfp = Range(2).map(i => 1);
xspp = 1;
cspp = 1;
gwp = 1;

topo = {
  name: 'canopy-dcomp',
  nodes: [
    ...minnows,
    ...ncps,
    ...sites,
    ...stors,
    ...emus
  ],
  switches: [
    ...xas,
    ...cas,
    ...xfs,
    ...cfs,
    xsp,
    csp,
    gw
  ],
  links: [
    ...minnows.map((m,i) => Link(m.name, 1, cas[i/3|0].name, cap[i/3|0]++)),
    ...minnows.map((m,i) => Link(m.name, 2, xas[i/3|0].name, xap[i/3|0]++)),
    ...ncps.map((n,i) => Link(n.name, 1, cas[i].name, cap[i]++)),
    ...ncps.map((n,i) => Link(n.name, 2, xas[i].name, xap[i]++)),
    ...ncps.map((n,i) => Link(n.name, 3, xas[i].name, xap[i]++)),

    ...cas.map((c,i) => Link(c.name, cap[i]++, cfs[i/2|0].name, cfp[i/2|0]++)),
    ...xas.map((x,i) => Link(x.name, xap[i]++, xfs[i/2|0].name, xfp[i/2|0]++)),

    ...cfs.map((c,i) => Link(c.name, cfp[i]++, csp.name, cspp++)),
    ...xfs.map((x,i) => Link(x.name, xfp[i]++, xsp.name, xspp++)),

    Link(csp.name, cspp++, gw.name, gwp++),
    Link(xsp.name, xspp++, gw.name, gwp++),

    Link(sites[0].name, 1, gw.name, gwp++),
    Link(sites[1].name, 1, gw.name, gwp++),

    ...stors.map((s,i) => Link(s.name, 1, cfs[i].name, cfp[i]++)),
    ...emus.map((e,i) => Link(e.name, 1, xfs[i].name, xfp[i]++))
  ]
};
