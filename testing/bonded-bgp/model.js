
function cumulus(name) {
  return {
    'name': name,
    'image': 'cumulusvx-3.5-mvrf',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': MB(512) },
    'mounts': [{ 'source': env.PWD+'/../../../canopy', 'point': '/tmp/canopy' }]
  };
}

function buster(name) {
  return {
    'name': name,
    'image': 'debian-buster',
    'os': 'linux',
    'cpu': { 'cores': 2 },
    'memory': { 'capacity': GB(1) },
    'mounts': [{ 'source': env.PWD+'/../../../canopy', 'point': '/tmp/canopy' }]
  };
}

topo = {
  name: 'canopy-bbgp',
  nodes: [buster('a'), buster('b'), buster('c'), buster('d')],
  switches: [cumulus('spine'), cumulus('leaf0'), cumulus('leaf1')],
  links: [
    Link('a', 1, 'leaf0', 1),
    Link('b', 1, 'leaf0', 2),
    Link('c', 1, 'leaf1', 1),
    Link('d', 1, 'leaf1', 2),
    Link('leaf0', 3, 'spine', 1),
    Link('leaf0', 4, 'spine', 2),
    Link('leaf1', 3, 'spine', 3),
    Link('leaf1', 4, 'spine', 4),
  ]
}
