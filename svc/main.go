package main

import (
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"
	canopy "gitlab.com/mergetb/tech/canopy/lib"
)

func main() {

	log.SetLevel(log.DebugLevel)
	log.Printf("canopy %s", canopy.Version)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGHUP)
	go func() {
		for {
			<-sigs
			log.Printf("handling hangup")
		}
	}()

	server := canopy.Server{}
	server.Run()

}
