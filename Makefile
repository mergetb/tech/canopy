prefix ?= /usr/local

# supported arch values are
#    -----------------
#    GOLANG | DEBIAN
#    -----------------
#    amd64    amd64
#    arm7     armhf
#    arm5     armel
arch ?= amd64
GOBIN ?= /tmp/canopy/tools

.tools:
	$(QUIET) mkdir .tools


.PHONY: all
all: build/canopy-server.$(arch) \
		 build/canopy.$(arch)

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

LIBSRC = lib/canopy.pb.go lib/common.go lib/client.go lib/config.go \
				 lib/server.go Makefile lib/cli_support.go \
				 lib/client-ops.go lib/server-ops.go lib/errors.go

CLIENTSRC = util/client/main.go util/client/show.go lib/canopy.pb.go

VERSION = $(shell git describe --always --long --dirty --tags)
LDFLAGS = "-X gitlab.com/mergetb/tech/canopy/lib.Version=$(VERSION)"

lib/canopy.pb.go: canopy.proto $(protoc-gen-go)
	$(protoc-build)
	$(QUIET) sed -i 's/`json:"-"`/`json:"-" yaml:"-"`/g' lib/canopy.pb.go

# amd64 / amd64
build/canopy-server.amd64: svc/main.go $(LIBSRC)
	$(QUIET) $(call go-build,amd64)

build/canopy.amd64: $(CLIENTSRC) $(LIBSRC)
	$(call go-build,amd64)

# arm7 / armhf
build/canopy-server.arm7: svc/main.go $(LIBSRC)
	$(call go-build-arm,7)

build/canopy.arm7: $(CLIENTSRC) $(LIBSRC)
	$(call go-build-arm,7)

# arm5 / armel
build/canopy-server.arm5: svc/main.go $(LIBSRC)
	$(call go-build-arm,5)

build/canopy.arm5: $(CLIENTSRC) $(LIBSRC)
	$(call go-build-arm,5)

.PHONY: clobber
clobber:
	$(QUIET) rm -rf \
		build/canopy.amd64 build/canopy-server.amd64 \
		build/canopy.arm7 build/canopy-server.arm7 \
		build/canopy.arm5 build/canopy-server.arm5 \
		lib/canopy.pb.go lib/client-functions.go

.PHONY: clean
clean:
	$(QUIET) rm -rf \
		build/canopy.$(arch) build/canopy-server.$(arch)

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) \
		GOOS=linux GOARCH=$1 \
		go build \
		-ldflags=${LDFLAGS} \
		-o $@ $(dir $<)*.go
endef

define go-build-arm
	$(call build-slug,go)
	$(QUIET) \
		GOOS=linux GOARCH=arm GOARM=$1 \
		go build \
		-ldflags=${LDFLAGS} \
		-o $@ $(dir $<)*.go
endef


define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./vendor \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:./lib
endef

build/canopy.bash_autocomplete: build/canopy.$(arch)
	build/canopy.$(shell ./local_arch.sh) autocomplete > $@

.PHONY: install
install: install-server install-client

.PHONY: install-server
install-server: build/canopy-server.$(arch)
	install -D $< $(DESTDIR)$(prefix)/bin/canopy-server

.PHONY: install-client
install-client: build/canopy.$(arch) build/canopy.bash_autocomplete
	install -D $< $(DESTDIR)$(prefix)/bin/canopy
	install -D build/canopy.bash_autocomplete $(DESTDIR)/usr/share/bash-completion/completions/canopy

.PHONY: distclean
distclean: clean
	$(QUIET) find lib -name "*.pb.go" -delete
	$(QUIET) rm -rf .tools
