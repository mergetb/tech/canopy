module gitlab.com/mergetb/tech/canopy

go 1.12

require (
	github.com/golang/protobuf v1.3.2
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/mergetb/tech/rtnl v0.1.8
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7
	golang.org/x/sys v0.0.0-20190830142957-1e83adbbebd0
	google.golang.org/grpc v1.23.0
	gopkg.in/yaml.v2 v2.2.2
)
