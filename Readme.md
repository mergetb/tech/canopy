# Canopy

Canopy is a virtual network framework. At it's core Canopy is an API with a
number of back end implementations and a command line client for convenience.
Canopy is designed manage large complex virtual networks across a large number
of devices, and do so with transactional semantics. Canopy allows you to build
up complex transactions across multiple devices that either completely
synthesize the desired virtual network or do nothing at all.

Consider a simple example of setting up a VXLAN across three switches

```shell
canopy set vtep vni-1010 1010 10 present      leaf1,leaf2,leaf3
canopy set port vlan-access 10 swp1           leaf1,leaf2,leaf3
canopy set vtep bridge-learning off vni-1010  leaf1,leaf2,leaf3
canopy set port mtu 9000 vni-1010             leaf1,leaf2,leaf3
canopy set vtep tunnelip 10.1.0.1 vni-1010    leaf1
canopy set vtep tunnelip 10.1.0.2 vni-1010    leaf2
canopy set vtep tunnelip 10.1.0.3 vni-1010    leaf3
```

All the above commands do is build up a transaction. You can inspect the
transaction through the `canopy pending` command

```yaml
leaf1:
  swp1:
    portvlanaccess: 10
  vni-1010:
    vtep: { presence: 1, vni: 1010, bridgeaccess: 10 }
    vtepbridgelearning: 2
    portmtu: 9000
    vteptunnelip: 10.1.0.1
leaf2:
  swp1:
    portvlanaccess: 10
  vni-1010:
    vtep: { presence: 1, vni: 1010, bridgeaccess: 10 }
    vtepbridgelearning: 2
    portmtu: 9000
    vteptunnelip: 10.1.0.2
leaf3:
  swp1:
    portvlanaccess: 10
  vni-1010:
    vtep: { presence: 1, vni: 1010, bridgeaccess: 10 }
    vtepbridgelearning: 2
    portmtu: 9000
    vteptunnelip: 10.1.0.3
```

A simple mash of `canopy commit` will run the transaction over all three
switches. Backing completely out if any fail.

## Server endpoints

By default canopy server listens on `0.0.0.0:6074`. From the server side, this can be
changed by using the `-endpoint` flag

```shell
canopy-server -endpoint 10.47.0.32:7744
```

On the client side the default port may be changed globally by setting the
environment variable `CANOPY_SERVER_PORT`. You can also specify the port to use
on a per server basis in canopy client commands, for example

```shell
canopy set port mtu 9000 vni-1010 leaf1:2299,leaf2:3388,leaf3:1144
```

## Implemented backends

The current backends that are supported are

- Cumulus Linux
- Debian Linux

Contributions for other backends are welcome.

## API

The primary goal in creating Canopy was not to create a nifty admin tool. But
rather to create a programmable framework for virtual networks across a wide
range of devices. A backend implementation is a matter of implementing the
Canopy API.

The Canopy API is deceptively simple at first glance.

```protobuf
service Canopy {
  rpc Set(SetRequest) returns (SetResponse);
  rpc Get(GetRequest) returns (GetResponse);
  rpc Rollback(RollbackRequest) returns (RollbackResponse);
}
```

Set and get requests are at the port level of granularity. Set requests accept a
list of PortElements.

```protobuf
message SetRequest {
  string clientId = 1;
  repeated PortElement elements = 2;
}
```

The `PortElement` is a polymorphic in the `Element` data type. Possible 
parameterizations are the following

```protobuf
message Element {
  oneof element {
    PortPresence    portPresence = 1;
    PortLink        portLink = 2;
    PortMtu         portMtu = 3;
    PortVlanAccess  portVlanAccess = 4;
    PortVlanTrunk   portVlanTrunk = 5;

    Vtep                vtep = 6;
    VtepTunnelIP        vtepTunnelIP = 7;
    VtepBridgeAccess    vtepBridgeAccess = 8;
    VtepBridgeLearning  vtepBridgeLearning = 9;
  }
}
```

## 2-Stage transactions

As Canopy expends in functionality so will the list parameterizable 
port properties. The thing that will remain the same is supporting transactions 
over sets of proerties over ports. The canopy transaction process is a two phase 
commit. In the first phase, each set of port parameters is applied on each switch
in a local transaction. When the result of these transactions comes back to the 
origniator, a second level transaction is formed over the local transactions. If 
all the local transactions were successful, the transaction process is complete. 
If any of the local transactions failed, the second phase transaction rolls back 
all first phase transactions.

## Adding a new backend

To add a new Canopy backend, you need to implement the `Platform` interface. As
Canopy evolves the list of methods to implement will also grow. Any interfaces
the platform cannot implement, or choses not to implement at the time can simply
return a `canopy.NotImplemented` error.

```go
type Platform interface {

	// Set the existence or absence for a set of ports
	SetPortPresence(port string, x PortPresence) error
	GetPortPresence(port string) (*PortPresence, error)

	// Set the link state (up or down) for a set of ports
	SetPortLink(port string, x PortLink) error
	GetPortLink(port string) (*PortLink, error)

	// Set the minimum transfer unit for a set of ports
	SetPortMtu(port string, x PortMtu) error
	GetPortMtu(port string) (*PortMtu, error)

	// Set the vlan-access vid on a port
	SetPortVlanAccess(port string, x PortVlanAccess) error
	GetPortVlanAccess(port string) (*PortVlanAccess, error)

	// Set the vlan-trunk vids on a port
	SetPortVlanTrunk(port string, x PortVlanTrunk) error
	GetPortVlanTrunk(port string) (*PortVlanTrunk, error)

	// Set the existence or absence of a vtep
	SetVtep(port string, x Vtep) error
	GetVtep(port string) (*Vtep, error)

	// Set a vtep tunnel-ip
	SetVtepTunnelIP(port string, x VtepTunnelIP) error
	GetVtepTunnelIP(port string) (*VtepTunnelIP, error)

	// Set vtep vtep device
	SetVtepDevice(port string, x string) error
	GetVtepDevice(port string) (string, error)

	// Set vtep bridge access
	SetVtepBridgeAccess(port string, x VtepBridgeAccess) error
	GetVtepBridgeAccess(port string) (*VtepBridgeAccess, error)

	// Set vtep bridge learning
	SetVtepBridgeLearning(port string, x VtepBridgeLearning) error
	GetVtepBridgeLearning(port string) (*VtepBridgeLearning, error)

	// Commit all pending changes
	Commit() error
}
```
