package main

import (
	"fmt"
	"os"
	"sort"
	"text/tabwriter"

	"github.com/fatih/color"
	"gitlab.com/mergetb/tech/canopy/lib"
)

var white = color.New(color.FgWhite).SprintFunc()
var green = color.New(color.FgGreen).SprintFunc()
var blue = color.New(color.FgBlue, color.Underline).SprintFunc()
var cyan = color.New(color.FgCyan).SprintFunc()
var red = color.New(color.FgRed).SprintFunc()
var gray = color.New(color.FgHiBlack).SprintFunc()

func showList(host string, elements []*canopy.Element) {

	sort.Slice(elements, func(i, j int) bool {
		return elements[i].Name < elements[j].Name
	})

	var ports []*canopy.Element
	var vteps []*canopy.Element
	var bridges []*canopy.Element

	for _, e := range elements {

		switch e.Value.(type) {

		case *canopy.Element_Port:
			ports = append(ports, e)

		case *canopy.Element_Vtep:
			vteps = append(vteps, e)

		case *canopy.Element_Bridge:
			bridges = append(bridges, e)
		}

	}

	var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

	fmt.Printf("%s\n", blue(host))

	fmt.Printf("%s\n", gray("physical"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\n",
		white("name"),
		"mtu",
		"bridge",
		"access",
		"tagged",
		"untagged",
	)
	for _, x := range ports {
		showPhysicalPort(x, tw)
	}
	tw.Flush()

	fmt.Printf("\n%s\n", gray("vteps"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		white("name"),
		"vni",
		"mtu",
		"access",
		"device",
		"bridge",
		"learning",
		"tunnel-ip",
		"access",
		"tagged",
		"untagged",
	)
	for _, x := range vteps {
		showVtep(x, tw)
	}
	tw.Flush()

	fmt.Printf("\n%s\n", gray("bridges"))
	for _, x := range bridges {
		showBridge(x, tw)
	}

}

func showQuery(cc *canopy.Client) {

	var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	for host, ports := range cc.Elements {
		fmt.Fprintf(tw, "%s\n", blue(host))
		for _, element := range ports {

			showPhysicalPort(element, tw)

		}
	}
	tw.Flush()

}

func showPhysicalPort(e *canopy.Element, tw *tabwriter.Writer) {

	p := e.GetPort()
	if p == nil {
		return
	}

	name := red(e.Name)
	if p.State == canopy.UpDown_Up {
		name = green(e.Name)
	}

	fmt.Fprintf(tw, "%s\t%d\t%s\t%d\t%v\t%v\n",
		name,
		p.Mtu,
		p.Bridge,
		p.Access,
		p.Tagged,
		p.Untagged,
	)
}

func showVtep(e *canopy.Element, tw *tabwriter.Writer) {

	v := e.GetVtep()

	name := red(e.Name)
	if v.State == canopy.UpDown_Up {
		name = green(e.Name)
	}

	fmt.Fprintf(tw, "%s\t%d\t%d\t%d\t%s\t%s\t%s\t%s\t%d\t%v\t%v\n",
		name,
		v.Vni,
		v.Mtu,
		v.BridgeAccess,
		v.Device,
		v.Bridge,
		canopy.OnOff_name[int32(v.Learning)],
		v.LocalIP,
		v.Access,
		v.Tagged,
		v.Untagged,
	)

}

func showBridge(e *canopy.Element, tw *tabwriter.Writer) {

	br := e.GetBridge()

	name := red(e.Name)
	if br.State == canopy.UpDown_Up {
		name = green(e.Name)
	}

	fmt.Fprintf(tw, "%s\n", name)

}
