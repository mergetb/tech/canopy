package main

import (
	"log"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	canopy "gitlab.com/mergetb/tech/canopy/lib"
)

func main() {

	log.SetFlags(0)

	rootCmd := &cobra.Command{
		Use:   "canopy",
		Short: "Canopy Virtual Network controller",
		Long:  "A command line utility to build virtual networks",
	}

	version := &cobra.Command{
		Use:   "version",
		Short: "Get canopy version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Print(canopy.Version)
		},
	}
	rootCmd.AddCommand(version)

	var completionCmd = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			rootCmd.GenBashCompletion(os.Stdout)
		},
	}
	rootCmd.AddCommand(completionCmd)

	/* =======================================================================
	 * set
	 * ======================================================================= */
	setCmd := &cobra.Command{
		Use:   "set",
		Short: "Set something",
	}
	rootCmd.AddCommand(setCmd)

	/* port ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	setPortCmd := &cobra.Command{
		Use:   "port",
		Short: "Set port properties",
		Args:  cobra.NoArgs,
	}
	setCmd.AddCommand(setPortCmd)

	setPortLink := &cobra.Command{
		Use:   "link [up|down] [port0,port1,...] [host0,host1,...]",
		Short: "Set port link state",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("link %v", args)

			value, err := canopy.ParseLinkState(args[0])
			if err != nil {
				log.Fatal(err)
			}

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			err = client().SetLinks(value, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	setPortCmd.AddCommand(setPortLink)

	setPortMtu := &cobra.Command{
		Use:   "mtu [value] [port0,port1,...] [host0,host1,...]",
		Short: "Set port mtu",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("mtu %v", args)

			value, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			err = client().SetMtus(int32(value), ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	setPortCmd.AddCommand(setPortMtu)

	var (
		absent bool
	)
	setPortBridge := &cobra.Command{
		Use:   "master [value] [port0,port1,...] [host0,host1,...]",
		Short: "Set port bridge",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("bridge %v", args)

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			presence := canopy.Presence_Present
			if absent {
				presence = canopy.Presence_Absent
			}

			master := &canopy.Master{
				Value:    args[0],
				Presence: presence,
			}

			err := client().SetMasters(master, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	setPortBridge.Flags().BoolVarP(&absent, "absent", "a", false, "remove vlans instead of create")
	setPortCmd.AddCommand(setPortBridge)

	setPortVlanAccess := &cobra.Command{
		Use:   "access [vid] [port0,port1,...] [host0,host1,...]",
		Short: "Set a port as a vlan access port",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("vlan-access %v", args)

			value, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			presence := canopy.Presence_Present
			if absent {
				presence = canopy.Presence_Absent
			}

			access := &canopy.Access{
				Vid:      int32(value),
				Presence: presence,
			}

			err = client().SetAccessPorts(access, ports, hosts).SavePending()

			if err != nil {
				log.Fatal(err)
			}

		},
	}
	setPortVlanAccess.Flags().BoolVarP(&absent, "absent", "a", false, "remove vlans instead of create")
	setPortCmd.AddCommand(setPortVlanAccess)

	setPortTagged := &cobra.Command{
		Use:   "tagged [vid0,vid1,...] [port0,port1,...] [host0,host1,...]",
		Short: "Set tagged vids",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			vids := strings.Split(args[0], ",")
			var ints []int32
			for _, s := range vids {
				i, err := strconv.Atoi(s)
				if err != nil {
					log.Fatalf("tags must be integers '%s'", s)
				}
				ints = append(ints, int32(i))
			}

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			info := &canopy.Tagged{
				Vids:     ints,
				Presence: canopy.Presence_Present,
			}
			if absent {
				info.Presence = canopy.Presence_Absent
			}

			err := client().SetTaggedPorts(info, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}

		},
	}
	setPortTagged.Flags().BoolVarP(&absent, "absent", "a", false, "remove vlans instead of create")
	setPortCmd.AddCommand(setPortTagged)

	setPortUntagged := &cobra.Command{
		Use:   "untagged [vid0,vid1,...] [port0,port1,...] [host0,host1,...]",
		Short: "Set untagged vids",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			vids := strings.Split(args[0], ",")
			var ints []int32
			for _, s := range vids {
				i, err := strconv.Atoi(s)
				if err != nil {
					log.Fatalf("tags must be integers '%s'", s)
				}
				ints = append(ints, int32(i))
			}

			ports, hosts := parsePortsAndHosts(args[1], args[2])

			info := &canopy.Untagged{
				Vids:     ints,
				Presence: canopy.Presence_Present,
			}
			if absent {
				info.Presence = canopy.Presence_Absent
			}

			err := client().SetUntaggedPorts(info, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}

		},
	}
	setPortUntagged.Flags().BoolVarP(&absent, "absent", "a", false, "remove vlans instead of create")
	setPortCmd.AddCommand(setPortUntagged)

	/* bridge ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	bridgeCmd := &cobra.Command{
		Use:   "bridge",
		Short: "manage bridges",
	}
	setCmd.AddCommand(bridgeCmd)

	bridgePresent := &cobra.Command{
		Use:   "present [bridge0,bridge1,...] [host0,host1,...]",
		Short: "ensure bridge is present",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("link %v", args)

			ports, hosts := parsePortsAndHosts(args[0], args[1])

			err := client().SetBridgesPresent(
				canopy.Presence_Present, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	bridgeCmd.AddCommand(bridgePresent)

	bridgeAbsent := &cobra.Command{
		Use:   "absent [bridge0,bridge1,...] [host0,host1,...]",
		Short: "ensure bridge is absent",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			ports, hosts := parsePortsAndHosts(args[0], args[1])

			err := client().SetBridgesPresent(
				canopy.Presence_Absent, ports, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	bridgeCmd.AddCommand(bridgeAbsent)

	/* vtep ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
	vtepCmd := &cobra.Command{
		Use:   "vtep",
		Short: "manage vteps",
	}
	setCmd.AddCommand(vtepCmd)

	var (
		parent   string
		learning bool
	)
	vtepPresent := &cobra.Command{
		Use:   "present <vni> <tunnelip> [vtep0,vtep1,...] [host0,host1,...]",
		Short: "ensure vtep is present",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {

			vni, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}
			if vni <= 1 {
				log.Fatal("must specify vni > 1")
			}

			ports, hosts := parsePortsAndHosts(args[2], args[3])

			vtep := &canopy.VtepPresence{
				Presence: canopy.Presence_Present,
				Vni:      int32(vni),
				TunnelIp: args[1],
				Parent:   parent,
				Learning: canopy.OnOff_Off,
			}

			if learning {
				vtep.Learning = canopy.OnOff_On
			}

			err = client().SetVteps(vtep, ports, hosts).SavePending()

			if err != nil {
				log.Fatal(err)
			}
		},
	}
	vtepPresent.Flags().StringVarP(&parent, "parent", "p", "", "physical device")
	vtepPresent.Flags().BoolVarP(&learning, "learning", "l", false, "enable MAC learning")
	vtepCmd.AddCommand(vtepPresent)

	vtepAbsent := &cobra.Command{
		Use:   "absent [vtep,vtep,...] [host0,host1,...]",
		Short: "ensure vtep is absent",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("link %v", args)

			vtep := &canopy.VtepPresence{
				Presence: canopy.Presence_Absent,
			}

			ports, hosts := parsePortsAndHosts(args[0], args[1])
			err := client().SetVteps(vtep, ports, hosts).SavePending()

			if err != nil {
				log.Fatal(err)
			}
		},
	}
	vtepAbsent.Flags().StringVarP(&parent, "parent", "p", "", "physical device")
	vtepCmd.AddCommand(vtepAbsent)

	getCmd := &cobra.Command{
		Use:   "get [port,...] [host,...]",
		Short: "get port info",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			c := client()

			ports, hosts := parsePortsAndHosts(args[0], args[1])
			for _, host := range hosts {
				c.GetPorts(ports, host)
			}
			err := c.Query()
			if err != nil {
				log.Fatal(err)
			}
			showQuery(c)
			log.Println("")

		},
	}
	rootCmd.AddCommand(getCmd)

	/*TODO break up

	var (
		vni      int
		vid      int
		state    string
		device   string
		bridge   string
		learning string
		ip       string
	)
	setVtepCmd := &cobra.Command{
		Use:   "vtep [port0,port1,...] [host0, host1,...]",
		Short: "Setup vteps",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("set-vtep %v", args)

			ports, hosts := parsePortsAndHosts(args[0], args[1])

			presence, err := canopy.ParsePresence(state)
			if err != nil {
				log.Fatal(err)
			}

			learning, err := canopy.ParseOnOff(learning)
			if err != nil {
				log.Fatal(err)
			}

			value := &canopy.VtepInfo{
				Presence:     presence,
				Vni:          int32(vni),
				BridgeAccess: int32(vid),
				Device:       device,
				Bridge:       bridge,
				Learning:     learning,
				LocalIP:      ip,
			}

			err = client().SetVtep(ports, value, hosts).SavePending()
			if err != nil {
				log.Fatal(err)
			}

		},
	}
	setVtepCmd.Flags().IntVarP(&vni, "vni", "v", 0, "virtual network identifier")
	setVtepCmd.Flags().IntVarP(&vid, "bridge-access", "a", 0, "bridge access vlan")
	setVtepCmd.Flags().StringVarP(&state, "state", "s", "present", "present or absent")
	setVtepCmd.Flags().StringVarP(&device, "device", "d", "", "physical device")
	setVtepCmd.Flags().StringVarP(&bridge, "bridge", "b", "", "bridge to attach to")
	setVtepCmd.Flags().StringVarP(&learning, "learning", "l", "off", "on or off")
	setVtepCmd.Flags().StringVarP(&ip, "ip", "i", "", "local tunnel ip")
	setCmd.AddCommand(setVtepCmd)
	*/

	/*
		setVtepLocalTunnelIpCmd := &cobra.Command{
			Use:   "tunnelip [ip] [vtep0,vtep1,...] [host0,host1,...]",
			Short: "Set tunnelip on vtep",
			Args:  cobra.ExactArgs(3),
			Run: func(cmd *cobra.Command, args []string) {
				log.Printf("set-vtep-local-tunnelip %v", args)

				ip := args[0]
				ports, hosts := parsePortsAndHosts(args[1], args[2])

				err := client().SetVtepTunnelIP(ports, ip, hosts).SavePending()
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		setVtepCmd.AddCommand(setVtepLocalTunnelIpCmd)
	*/

	/*
		setVtepBridgeAccessCmd := &cobra.Command{
			Use:   "bridge-access [vid] [bridge] [vtep0,vtep1,...] [host0,host1,...]",
			Short: "Set vtep bridge access",
			Args:  cobra.ExactArgs(4),
			Run: func(cmd *cobra.Command, args []string) {
				log.Printf("set-vtep-bridge-access %v", args)

				vid, err := strconv.Atoi(args[0])
				if err != nil {
					log.Fatal("bridge vlan id (vid) must be an integer")
				}
				bridge := args[1]
				ports, hosts := parsePortsAndHosts(args[2], args[3])

				vbr := &canopy.VtepBridgeAccessInfo{
					Vid:    int32(vid),
					Bridge: bridge,
				}

				err = client().SetVtepBridgeAccess(ports, vbr, hosts).SavePending()
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		setVtepCmd.AddCommand(setVtepBridgeAccessCmd)
	*/

	/*
		setVtepDeviceCmd := &cobra.Command{
			Use:   "device [device] [vtep0,vtep1,...] [host0,host1,...]",
			Short: "Set vtep bridge access",
			Args:  cobra.ExactArgs(3),
			Run: func(cmd *cobra.Command, args []string) {
				log.Printf("set-vtep-device %v", args)

				dev := args[0]
				ports, hosts := parsePortsAndHosts(args[1], args[2])

				err := client().SetVtepDevice(ports, dev, hosts).SavePending()
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		setVtepCmd.AddCommand(setVtepDeviceCmd)
	*/

	/*
		setVtepBridgeLearning := &cobra.Command{
			Use:   "bridge-learning [on|off] [vtep0,vtep1,...] [host0,host1,...]",
			Short: "Setup bridge learning on vtep",
			Args:  cobra.ExactArgs(3),
			Run: func(cmd *cobra.Command, args []string) {
				log.Printf("set-vtep-bridge-learning %v", args)

				value, err := canopy.ParseOnOff(args[0])
				if err != nil {
					log.Fatal(err)
				}
				ports, hosts := parsePortsAndHosts(args[1], args[2])

				err = client().SetVtepBridgeLearning(ports, value, hosts).SavePending()
				if err != nil {
					log.Fatal(err)
				}
			},
		}
		setVtepCmd.AddCommand(setVtepBridgeLearning)
	*/

	/* =======================================================================
	 * get
	 * ======================================================================= */

	/*
		getCmd := &cobra.Command{
			Use:   "get",
			Short: "Get something",
		}
		rootCmd.AddCommand(getCmd)
	*/

	/* port ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	/*
		getPortCmd := &cobra.Command{
			Use:   "port [port0,port1,...] [host0,host1,...]",
			Short: "Get port status",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPortPresence(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getCmd.AddCommand(getPortCmd)

		getPortLinkCmd := &cobra.Command{
			Use:   "link [port0,port1,...] [host0,host1,...]",
			Short: "Get port link",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPortLink(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getPortCmd.AddCommand(getPortLinkCmd)

		getPortMtuCmd := &cobra.Command{
			Use:   "mtu [port0,port1,...] [host0,host1,...]",
			Short: "Get port mtu",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPortMtu(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getPortCmd.AddCommand(getPortMtuCmd)

		getPortVlanAccessCmd := &cobra.Command{
			Use:   "vlan-access [port0,port1,...] [host0,host1,...]",
			Short: "Get port access vni",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPortVlanAccess(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getPortCmd.AddCommand(getPortVlanAccessCmd)

		getPortVlanTrunkCmd := &cobra.Command{
			Use:   "vlan-trunk [port0,port1,...] [host0,host1,...]",
			Short: "Get port trunk vnis",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPortVlanTrunk(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getPortCmd.AddCommand(getPortVlanTrunkCmd)
	*/

	listCmd := &cobra.Command{
		Use:   "list [host0,host1,...]",
		Short: "Get portss",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			hosts := strings.Split(args[0], ",")
			if len(hosts) == 0 {
				log.Fatal("host names are required")
			}

			lists, err := canopy.ListPorts(hosts)
			if err != nil {
				log.Fatal(err)
			}

			//canopy.ShowResult(data)
			for host, data := range lists {
				showList(host, data)
				log.Println("")
			}
			//canopy.ShowErrors(errors)

		},
	}
	rootCmd.AddCommand(listCmd)

	/*
		getPortCmd := &cobra.Command{
			Use:   "port [name0,name1,...] [host0,host1,...]",
			Short: "Get portss",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetPhysicalPort(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getCmd.AddCommand(getPortCmd)

		getVtepCmd := &cobra.Command{
			Use:   "vtep [name0,name1,...] [host0,host1,...]",
			Short: "Get vteps",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetVtep(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getCmd.AddCommand(getVtepCmd)
	*/

	/*
		getVtepTunnelIPCmd := &cobra.Command{
			Use:   "tunnelip [name0,name1,....] [host0,host1,...]",
			Short: "Get vtep local tunnel ips",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetVtepTunnelIP(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getVtepCmd.AddCommand(getVtepTunnelIPCmd)
	*/

	/*
		getVtepDeviceCmd := &cobra.Command{
			Use:   "device [name0,name1,....] [host0,host1,...]",
			Short: "Get vtep device",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetVtepDevice(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getVtepCmd.AddCommand(getVtepDeviceCmd)
	*/

	/*
		getVtepBridgeAccessCmd := &cobra.Command{
			Use:   "bridge-access [name0,name1,....] [host0,host1,...]",
			Short: "Get vtep bridge access",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetVtepBridgeAccess(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getVtepCmd.AddCommand(getVtepBridgeAccessCmd)
	*/

	/*
		getVtepBridgeLearning := &cobra.Command{
			Use:   "bridge-learning [name0,name1,....] [host0,host1,...]",
			Short: "Get vtep bridge-learning status",
			Args:  cobra.ExactArgs(2),
			Run: func(cmd *cobra.Command, args []string) {

				ports, hosts := parsePortsAndHosts(args[0], args[1])
				data, errors := canopy.GetVtepBridgeLearning(ports, hosts)

				canopy.ShowResult(data)
				canopy.ShowErrors(errors)

			},
		}
		getVtepCmd.AddCommand(getVtepBridgeLearning)
	*/

	/* pending ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	pendingCmd := &cobra.Command{
		Use:   "pending",
		Short: "Show pending commands",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {

			cli, err := canopy.LoadClient()
			if err != nil {
				log.Fatal(err)
			}

			buf, err := yaml.Marshal(cli)
			if err != nil {
				log.Fatal(err)
			}

			log.Print(string(buf))

		},
	}
	rootCmd.AddCommand(pendingCmd)

	/* commit ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	commitCmd := &cobra.Command{
		Use:   "commit",
		Short: "Commit pending commands",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {

			log.Printf("commit %v", args)

			err := client().Commit()
			if err != nil {
				log.Fatal(err)
			}
			canopy.ClearPending()

		},
	}
	rootCmd.AddCommand(commitCmd)

	/* abort ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

	abortCmd := &cobra.Command{
		Use:   "abort",
		Short: "Abort pending commands",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {

			log.Printf("abort %v", args)
			err := canopy.ClearPending()
			if err != nil {
				log.Fatal(err)
			}

		},
	}
	rootCmd.AddCommand(abortCmd)

	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}

}

func client() *canopy.Client {
	c, err := canopy.LoadClient()
	if err != nil {
		log.Fatal(err)
	}
	return c
}

func parsePortsAndHosts(ports, hosts string) ([]string, []string) {

	ps := strings.Split(ports, ",")
	if len(ports) == 0 {
		log.Fatal("port names are required")
	}

	hs := strings.Split(hosts, ",")
	if len(hosts) == 0 {
		log.Fatal("host names are required")
	}

	return ps, hs

}

func sortedMapKeys(m interface{}) (keyList []string) {
	keys := reflect.ValueOf(m).MapKeys()

	for _, key := range keys {
		keyList = append(keyList, key.Interface().(string))
	}
	sort.Strings(keyList)
	return
}

func intsToString(ints []int32) string {
	var ss []string
	for _, i := range ints {
		ss = append(ss, strconv.Itoa(int(i)))
	}
	return strings.Join(ss, ",")
}
